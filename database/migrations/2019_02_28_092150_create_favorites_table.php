<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('skater_id');
            $table->integer('trick_id');
            $table->timestamps();
        });
        // Schema::table('favorites', function (Blueprint $table) {
        //     $table->foreign('skater_id', 'fk_tutorials_skater')->references('id')->on('skaters')->onDelete('cascade');
        //     $table->foreign('trick_id', 'fk_tutorials_trick')->references('id')->on('tricks')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorites');
    }
}
