<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTricksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tricks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('trick_name');
            $table->text('description');
            $table->text('where_to_use');
            $table->integer('skater_id')->unsigned();
            //$table->foreign('skater_id')->references('id')->on('skaters')->onDelete('cascade');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
        // Schema::table('tricks', function (Blueprint $table) {
        //     $table->foreign('skater_id', 'fk_tutorials_skater')->references('id')->on('skaters')->onDelete('cascade');
        // });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tricks');
    }
}
