<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    //
    protected $fillable = ['skater_id', 'trick_id'];

    public function skater()
    {
      return $this->belongsTo(Skater::class);
    }
    public function tricks()
    {
      return $this->belongsTo(Trick::class);
    }
}
