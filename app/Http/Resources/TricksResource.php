<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TricksResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'trick_name' => $this->trick_name,
            'description' => $this->description,
            'where_to_use' => $this->where_to_use,
            'skater_id' => $this->skater_id
          ];
    }
}
