<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trick extends Model
{
    //
    protected $fillable = ['trick_name', 'where_to_use', 'description', 'skater_id'];

    public function skater()
    {
      return $this->belongsTo(Skater::class);
    }
}
